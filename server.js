const express = require("express")
const { sequelize } = require("./models")
const mainRouter = require("./app/router/router")
const passport = require("passport")
const middlewares = require("./app/middlewares/middlewares")

const PORT = 3003
const app = express()

middlewares.forEach((middleware) => app.use(middleware))
require("./passport")(passport)
app.use(passport.initialize())
app.use(passport.session())
app.use(mainRouter)

const start = async () => {
    try {
        await sequelize.sync({
            alter: true, 
            force: false
        })
        await sequelize.authenticate()
        app.listen(PORT, () => {
            console.log(`${PORT} started server`)
        })
    } catch (error) {
        console.error(error)
    }
}
start().catch(console.error)