const LocalStrategy = require("passport-local").Strategy
const bcrypt = require("bcryptjs")
const { User } = require("./models")

module.exports = function (passport) {
    passport.serializeUser((users, done) => done(null, users.id))

    passport.deserializeUser((id, done) => {
        User.findOne({where: {id}}).then((users) => done(null, users))
    })

    passport.use(
        "local-signin",
        new LocalStrategy(
            {
                usernameField: "username",
                passwordField: "password",
                passReqToCallback: true
            },
            function(req, username, password, done) {
                const isValidPassword = function (userpass, password) {
                    return bcrypt.compareSync(password, userpass)
                }
                
                User.findOne({where: {username}})
                    .then((user) => {
                        if (!user) {
                            return done(null, false, { message: 'Incorrect username or password.' })
                        }
                        if (!isValidPassword(user.password, password)) {
                            return done(null, false, { message: 'Incorrect username or password.' })
                        }
                        const userinfo = user.get()
                        req.logIn(user, (err) => (err ? next(err) : null))
                        return done(null, userinfo)
                    })
                    .catch(err => {
                        console.error(err)
                        return done(null, false, { message: 'Login error' })
                    })
            }
        )
    )
}
