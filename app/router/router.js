const { Router } = require("express")
const users = require("./users/usersRouter")
const pictures = require("./pictures/picturesRouters")
const mainRouter = Router()

mainRouter.use("/users", users)
mainRouter.use("/pictures", pictures)

module.exports = mainRouter