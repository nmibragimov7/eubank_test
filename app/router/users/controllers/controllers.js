const { User } = require("../../../../models")

const usersControllers = {
    async getUser(req, res) {
        try {
            const user = await User.findOne({
                where: {
                    id: req.user.id
                }
            })
            res.send(user)
        } catch (error) {
            res.status(401).send(error)
        }
        
    },
    async getUsers(req, res) {
        try {
            const users = await User.findAll()
            res.send(users)
        } catch (error) {
            res.status(401).send(error)
        }
        
    },
    async createUser(req, res) {
        try {
            const {
                username,
                email,
                password,
            } = req.body
            User.create({
                username,
                email,
                password,
            })
                .then(user => {
                    res.status(200).send(user)
                })
                .catch(e => {
                    res.status(400).send(e)
                })
        } catch (e) {
            res.status(401).send(e)
        }
    },
    async createSessions(req, res) {
        try {
            const user = await User.findOne({
                where: {id: req.user.id},
            });
            res.send(user);
        } catch (e) {
            res.status(401).send(e);
        }
    },
    async deleteSessions(req, res) {
        try {
            await req.session.destroy();
            res.send({message: "Logout success"});
        } catch (e) {
            res.status(401).send(e);
        }
    }
}

module.exports = usersControllers