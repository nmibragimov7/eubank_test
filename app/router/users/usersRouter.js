const usersControllers = require("./controllers/controllers")
const express = require("express")
const passport = require("passport")

const users = express.Router()

users.get("/current", usersControllers.getUser)
users.get("/", usersControllers.getUsers)
users.post("/", usersControllers.createUser)
users.delete("/sessions", usersControllers.deleteSessions);
users.post("/sessions", (req, res) => {
    passport.authenticate("local-signin", (err, user, info) => {
        if (info) return res.status(400).send({...info})
        usersControllers.createSessions(req, res)
    })(req, res)
})

module.exports = users