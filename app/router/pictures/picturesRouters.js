const express = require("express")
const picturesControllers = require("./controllers/controllers")
const auth = require("../../middlewares/auth")

const pictures = express.Router()

pictures.get("/", [auth], picturesControllers.getPictures)
pictures.get("/:id", [auth], picturesControllers.getPicture)
pictures.post("/", [auth], picturesControllers.addPictures)

module.exports = pictures