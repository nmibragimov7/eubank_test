const { Picture } = require("../../../../models")
const { nanoid } = require("nanoid")
const path = require('path')

const picturesControllers = {
    async addPictures(req, res) {
        try {
            let uploadPath = null
            if(!req.files) return res.status(404).send({message: "Выберите картинки"})
            let files = req.files && req.files.picture
            files.forEach(file => {
                let title = null
                if(file) {
                    title = nanoid() + path.extname(file.name)
                    uploadPath = './public/photos/' + title
        
                    file.mv(uploadPath, function(err) {
                        if (err)
                        return res.status(500).send(err)
                    })
                }
                Picture.create({
                    title: title,
                    userId: req.user.id,
                })
            })
            res.status(200).send({message: "Картинки добавлены"})
        } catch (e) {
            res.status(401).send(e)
        }
    },
    async getPictures(req, res) {
        try {
            const pictures = await Picture.findAll({
                where: {
                    userId: req.user.id
                },
            })
            if (!pictures) return res.sendStatus(404)
            res.send(pictures)
        } catch (e) {
            res.status(401).send(e)
        }
    },
    async getPicture(req, res) {
        try {
            const { id } = req.params
            const picture = await Picture.findOne({
                where: {
                    id,
                    userId: req.user.id
                },
            })
            if (!picture) return res.sendStatus(404)
            res.send(picture)
        } catch (e) {
            res.status(401).send(e)
        }
    }
}

module.exports = picturesControllers