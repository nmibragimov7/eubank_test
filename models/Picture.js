const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Picture extends Model {
        static associate({ User }) {
            this.belongsTo(User, {
                foreignKey: "userId",
                as: "user",
                onDelete: 'CASCADE'
            })
        }
    }
    Picture.init(
        {
            userId: {
                type: DataTypes.INTEGER,
                defaultValue: null
            },
            title: {
                type: DataTypes.STRING(255),
                allowNull: false,
            }
        },
        { 
            sequelize, 
            modelName: 'Picture', 
            tableName: "pictures" 
        }
    )

    return Picture
}
