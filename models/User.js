const { Model } = require('sequelize');
const bcrypt = require("bcryptjs")

module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        static associate({ Picture }) {
            this.hasMany(Picture, {
                foreignKey: "userId",
                as: "user",
            })
        }
        toJSON() {
            return {...this.get(), password: null};
        }
    }
    User.init(
        {
            username: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    async checkUnique(username) {
                        const user = await User.findOne({where: {username}});
                        if (user) throw new Error("Пользователь уже существует");
                    },
                },
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            }
        },
        { 
            sequelize, 
            timestamps: false,
            modelName: 'User', 
            tableName: "users" 
        }
    )
    User.beforeCreate((user) =>
        bcrypt
            .hash(user.password, 10)
            .then((hashPass) => {
                user.password = hashPass;
            })
            .catch((err) => {
                throw new Error(err);
            })
    );
    return User
}
