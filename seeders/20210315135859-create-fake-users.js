const bcrypt = require("bcryptjs")

module.exports = {
  up: async (queryInterface) => {
    const pass1 = "user";
    const pass2 = "admin";
    const hashPass1 = await bcrypt.hash(pass1, 10);
    const hashPass2 = await bcrypt.hash(pass2, 10);
    const users = [
      {
        username: "user",
        password: hashPass1,
      },
      {
        username: "admin",
        password: hashPass2,
      }
    ];
    await queryInterface.bulkInsert("users", users, {})
  },

  down: async () => {}
}