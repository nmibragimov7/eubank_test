module.exports = {
    up: async (queryInterface, Sequelize) => {
        const pictures = [
        {
            userId: 1,
            title: "icon.png",
            createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
            updatedAt: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        {
            userId: 2,
            title: "icon.png",
            createdAt: Sequelize.literal('CURRENT_TIMESTAMP'),
            updatedAt: Sequelize.literal('CURRENT_TIMESTAMP')
        }
        ];
        await queryInterface.bulkInsert("pictures", pictures, {})
    },

    down: async () => {}
}