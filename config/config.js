require("dotenv").config();

module.exports = {
  development: {
    username: process.env.MYSQL_USER || "esdp-user",
    password: process.env.MYSQL_PASSWORD || "esdp-user",
    database: process.env.MYSQL_DATABASE || "eurasiabank",
    host: "127.0.0.1",
    dialect: "mysql"
  },
  test: {
    username: "root",
    password: null,
    database: "exam13_test",
    host: "127.0.0.1",
    dialect: "mysql"
  },
  production: {
    username: "root",
    password: null,
    database: "exam13_production",
    host: "127.0.0.1",
    dialect: "mysql"
  }
}
